%%%%
%%%% Test of primitive related with Elab_Context
%%%%

test-isbound = do {
  Test_info "ELABCTX" "isbound";

  env <- Elab_getenv ();

  r0 <- Test_true "Int" (Elab_isbound "Int" env);
  r1 <- Test_true "List" (Elab_isbound "List" env);
  r2 <- Test_true "Elab_isbound" (Elab_isbound "Elab_isbound" env);
  r3 <- Test_true "do" (Elab_isbound "do" env);
  r4 <- Test_true "cons" (Elab_isbound "cons" env);
  r5 <- Test_true "nil" (Elab_isbound "nil" env);
  r6 <- Test_true "true" (Elab_isbound "true" env);
  r7 <- Test_true "false" (Elab_isbound "false" env);
  r8 <- Test_true "none" (Elab_isbound "none" env);
  r9 <- Test_false "randomunusednameabc123" (Elab_isbound "randomunusednameabc123" env);
  r10 <- Test_false " " (Elab_isbound " " env);

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 (and r5
                       (and r6 (and r7 (and r8 (and r9 r10))))))))));

  if success then
    (Test_info "ELABCTX" "test on isbound succeeded")
  else
    (Test_warning "ELABCTX" "test on isbound failed");

  IO_return success;
};

test-isconstructor = do {
  Test_info "ELABCTX" "isconstructor";

  env <- Elab_getenv ();

  r0 <- Test_false "Int" (Elab_isconstructor "Int" env);
  r1 <- Test_false "List" (Elab_isconstructor "List" env);
  r2 <- Test_false "Elab_isconstructor" (Elab_isconstructor "Elab_isconstructor" env);
  r3 <- Test_false "do" (Elab_isconstructor "do" env);
  r4 <- Test_true "cons" (Elab_isconstructor "cons" env);
  r5 <- Test_true "nil" (Elab_isconstructor "nil" env);
  r6 <- Test_true "true" (Elab_isconstructor "true" env);
  r7 <- Test_true "false" (Elab_isconstructor "false" env);
  r8 <- Test_true "none" (Elab_isconstructor "none" env);
  r9 <- Test_false "randomunusednameabc123" (Elab_isconstructor "randomunusednameabc123" env);
  r10 <- Test_false " " (Elab_isconstructor " " env);

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 (and r5
                       (and r6 (and r7 (and r8 (and r9 r10))))))))));

  if success then
    (Test_info "ELABCTX" "test on isconstructor succeeded")
  else
    (Test_warning "ELABCTX" "test on isconstructor failed");

  IO_return success;
};

test-is-nth-erasable = do {
  Test_info "ELABCTX" "is-nth-erasable";

  env <- Elab_getenv ();

  r0 <- Test_true "ctor1 0" (Elab_is-nth-erasable "ctor1" 0 env);
  r1 <- Test_true "ctor1 1" (Elab_is-nth-erasable "ctor1" 1 env);
  r2 <- Test_true "ctor1 2" (Elab_is-nth-erasable "ctor1" 2 env);

  r3 <- Test_false "ctor2 0" (Elab_is-nth-erasable "ctor2" 0 env);
  r4 <- Test_false "ctor2 1" (Elab_is-nth-erasable "ctor2" 1 env);
  r5 <- Test_false "ctor2 2" (Elab_is-nth-erasable "ctor2" 2 env);

  r6 <- Test_false "ctor3 0" (Elab_is-nth-erasable "ctor3" 0 env);
  r7 <- Test_false "ctor3 1" (Elab_is-nth-erasable "ctor3" 1 env);
  r8 <- Test_false "ctor3 2" (Elab_is-nth-erasable "ctor3" 2 env);

  r9 <- Test_true "ctor4 0" (Elab_is-nth-erasable "ctor4" 0 env);
  r10 <- Test_false "ctor4 1" (Elab_is-nth-erasable "ctor4" 1 env);
  r11 <- Test_false "ctor4 2" (Elab_is-nth-erasable "ctor4" 2 env);

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 (and r5
                       (and r6 (and r7 (and r8 (and r9 (and r10 r11)))))))))));

  if success then
    (Test_info "ELABCTX" "test on is-nth-erasable succeeded")
  else
    (Test_warning "ELABCTX" "test on is-nth-erasable failed");

  IO_return success;
};

test-is-arg-erasable = do {
  Test_info "ELABCTX" "is-arg-erasable";

  env <- Elab_getenv ();

  r0 <- Test_true "ctor1 0" (Elab_is-arg-erasable "ctor1" "a" env);
  r1 <- Test_true "ctor1 1" (Elab_is-arg-erasable "ctor1" "b" env);
  r2 <- Test_true "ctor1 2" (Elab_is-arg-erasable "ctor1" "c" env);

  r3 <- Test_false "ctor2 0" (Elab_is-arg-erasable "ctor2" "a" env);
  r4 <- Test_false "ctor2 1" (Elab_is-arg-erasable "ctor2" "b" env);
  r5 <- Test_false "ctor2 2" (Elab_is-arg-erasable "ctor2" "c" env);

  r6 <- Test_false "ctor3 0" (Elab_is-arg-erasable "ctor3" "a" env);
  r7 <- Test_false "ctor3 1" (Elab_is-arg-erasable "ctor3" "b" env);
  r8 <- Test_false "ctor3 2" (Elab_is-arg-erasable "ctor3" "c" env);

  r9 <- Test_true "ctor4 0" (Elab_is-arg-erasable "ctor4" "a" env);
  r10 <- Test_false "ctor4 1" (Elab_is-arg-erasable "ctor4" "b" env);
  r11 <- Test_false "ctor4 2" (Elab_is-arg-erasable "ctor4" "c" env);

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 (and r5
                       (and r6 (and r7 (and r8 (and r9 (and r10 r11)))))))))));

  if success then
    (Test_info "ELABCTX" "test on is-arg-erasable succeeded")
  else
    (Test_warning "ELABCTX" "test on is-arg-erasable failed");

  IO_return success;
};

test-nth-arg = do {
  Test_info "ELABCTX" "nth-arg";

  env <- Elab_getenv ();

  r0 <- Test_eq "ctor4 0" (Elab_nth-arg "ctor4" 0 env) (some "a");
  r1 <- Test_eq "ctor4 1" (Elab_nth-arg "ctor4" 1 env) (some "b");
  r2 <- Test_eq "ctor4 2" (Elab_nth-arg "ctor4" 2 env) (some "c");

  r0 <- Test_eq "ctor1 0" (Elab_nth-arg "ctor1" 0 env) (some "a");
  r1 <- Test_eq "ctor1 1" (Elab_nth-arg "ctor1" 1 env) (some "b");
  r2 <- Test_eq "ctor1 2" (Elab_nth-arg "ctor1" 2 env) (some "c");

  %% argument without name
  r3 <- Test_eq "cons 0" (Elab_nth-arg "cons" 0 env) none;
  r4 <- Test_eq "cons 1" (Elab_nth-arg "cons" 1 env) none;

  %% no argument
  r5 <- Test_eq "nil 0" (Elab_nth-arg "nil" 0 env) none;

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 r5)))));

  if success then
    (Test_info "ELABCTX" "test on nth-arg succeeded")
  else
    (Test_info "ELABCTX" "test on nth-arg failed");

  IO_return success;
};

test-arg-pos = do {
  Test_info "ELABCTX" "arg-pos";

  env <- Elab_getenv ();

  r0 <- Test_eq "ctor4 a" (Elab_arg-pos "ctor4" "a" env) (some 0);
  r1 <- Test_eq "ctor4 b" (Elab_arg-pos "ctor4" "b" env) (some 1);
  r2 <- Test_eq "ctor4 c" (Elab_arg-pos "ctor4" "c" env) (some 2);

  r0 <- Test_eq "ctor1 a" (Elab_arg-pos "ctor1" "a" env) (some 0);
  r1 <- Test_eq "ctor1 b" (Elab_arg-pos "ctor1" "b" env) (some 1);
  r2 <- Test_eq "ctor1 c" (Elab_arg-pos "ctor1" "c" env) (some 2);

  %% argument without name
  r3 <- Test_eq "cons foo" (Elab_arg-pos "cons" "foo" env) none;
  r4 <- Test_eq "cons bar" (Elab_arg-pos "cons" "bar" env) none;

  %% no argument
  r5 <- Test_eq "nil bidon" (Elab_arg-pos "nil" "bidon" env) none;

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 r5)))));

  if success then
    (Test_info "ELABCTX" "test on arg-pos succeeded")
  else
    (Test_info "ELABCTX" "test on arg-pos failed");

  IO_return success;
};

exec-test = do {
  b1 <- test-isbound;
  b2 <- test-isconstructor;
  b3 <- test-is-nth-erasable;
  b4 <- test-is-arg-erasable;
  b5 <- test-nth-arg;
  b6 <- test-arg-pos;

  IO_return (and b1 (and b2 (and b3 (and b4 (and b5 b6)))));
};
