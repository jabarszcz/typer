%%% plain-let.typer --- Basic "parallel" `let`

%% List_nth     = list.nth;
%% List_fold2   = list.fold2;
%% List_map     = list.map;
%% List_foldl   = list.foldl;
%% List_reverse = list.reverse;
%% List_tail    = list.tail;

%%
%% The idea is to use unique identifier in a first let
%%   and then assign those unique identifier to the original symbol
%%
%% Useful for auto-generated code (macro, ...)
%%

%%
%% Takes the plain argument of macro `plain-let`
%% Returns the `plain-let` code
%%
impl : List Sexp -> IO Sexp;
impl args = let

  %% Error use in Sexp_dispatch
  serr = lambda _ -> Sexp_error;

  %% Takes an assignment statement and return a new
  %%   symbol for the resulting variable
  %% For function it only rename the function name and not the argument
  gen-sym : Sexp -> IO Sexp;
  gen-sym arg = let

    %% error use in Sexp_dispatch
    io-serr = lambda _ -> IO_return Sexp_error;

    %% Get a `gensym` symbol
    %% It can rename a function name (not the argument)
    %%   or just a symbol
    rename : Sexp -> IO Sexp;
    rename sexp = Sexp_dispatch sexp
      (lambda _ ss -> do {
        name <- gensym ();
        IO_return (Sexp_node name ss);
      })
      (lambda _ -> gensym ())
      io-serr io-serr io-serr io-serr;

  in Sexp_dispatch arg
    (lambda s ss -> if (Sexp_eq s (Sexp_symbol "_=_")) then
      (rename (List_nth 0 ss Sexp_error)) else
      (IO_return Sexp_error))
    io-serr io-serr io-serr io-serr io-serr;

  %% Takes an assignment statement and return the right-hand side
  get-def : Sexp -> Sexp;
  get-def arg = Sexp_dispatch arg
    (lambda s ss -> if (Sexp_eq s (Sexp_symbol "_=_")) then
      (List_nth 1 ss Sexp_error) else
      (Sexp_error))
    serr serr serr serr serr;

  %% Takes an assignment statement and return the left-hand side
  get-var : Sexp -> Sexp;
  get-var arg = Sexp_dispatch arg
    (lambda s ss -> if (Sexp_eq s (Sexp_symbol "_=_")) then
      (List_nth 0 ss Sexp_error) else
      (Sexp_error))
    serr serr serr serr serr;

  %% Takes a list of assignment and return a list of pair of `gensym` and definition (rhs)
  get-sym-def : List Sexp -> IO (List (Pair Sexp Sexp));
  get-sym-def args = do {
    r <- List_foldl (lambda o arg -> do {
      o <- o;
      sym <- gen-sym arg;
      def <- IO_return (get-def arg);
      IO_return (cons (pair sym def) o); % order doesn't matter
    }) (IO_return nil) args;
    IO_return r;
  };

  %% Takes a list of assignment and the output of `get-sym-def`
  %% Returns a list of pair of `gensym` and original symbol
  get-var-sym : List Sexp -> List (Pair Sexp Sexp) -> IO (List (Pair Sexp Sexp));
  get-var-sym args syms = let

    ff : IO (List (Pair Sexp Sexp)) -> Pair Sexp Sexp -> Sexp -> IO (List (Pair Sexp Sexp));
    ff o p arg = do {
      o <- o;
      sym <- IO_return (case p | pair s _ => s);
      var <- IO_return (get-var arg);
      IO_return (cons (pair var sym) o);
    };

  in do {
    r <- List_fold2 ff (IO_return nil) syms args;
    IO_return r;
  };

  %% Takes a list of assignment and a body (likely using those assignment)
  %% Returns a `let [assignment] in [body]` construction
  let-in : Sexp -> Sexp -> Sexp;
  let-in decls body = Sexp_node (Sexp_symbol "let_in_") (cons decls (cons body nil));

  %% Takes a list of assignment and separate them with `;`
  let-decls : List Sexp -> Sexp;
  let-decls decls = Sexp_node (Sexp_symbol "_;_") decls;

  %% Takes lists of pairs (`gensym`,definition) and (`gensym`,original symbol)
  %%     and the body (likely using original symbol)
  %% Returns
  %%     let [gensym] = [definition] in let [original symbol] = [gensym] in [body]
  gen-code : List (Pair Sexp Sexp) -> List (Pair Sexp Sexp) -> Sexp -> Sexp;
  gen-code sym-def var-sym body = let

    mf : Pair Sexp Sexp -> Sexp;
    mf p = case p | pair s0 s1 =>
      Sexp_node (Sexp_symbol "_=_") (cons s0 (cons s1 nil));

    decls0 = List_map mf sym-def;

    decls1 = List_map mf var-sym;

  in let-in (let-decls decls0) (let-in (let-decls decls1) body);

  %% Returns a list of assignment from a "_;_"-node
  get-decls : Sexp -> List Sexp;
  get-decls sexp = let

    xserr = lambda _ -> (nil : List Sexp);

  in Sexp_dispatch sexp
    (lambda s ss -> if (Sexp_eq s (Sexp_symbol "_;_")) then
      (ss) else
      (nil))
    xserr xserr xserr xserr xserr;

in do {
  %% get list of pair and use it to generate `plain-let`

  defs <- IO_return (get-decls (List_nth 0 args Sexp_error));
  body <- IO_return (List_nth 1 args Sexp_error);
  sym-def <- get-sym-def defs;
  sym-def <- IO_return (List_reverse (List_tail sym-def) nil);
  var-sym <- get-var-sym defs sym-def;
  IO_return (gen-code sym-def var-sym body);
};

%% just calling the `impl` function in a macro
plain-let-macro = macro (lambda args -> do {
  r <- impl args;
  %%     Sexp_debug_print r;
  IO_return r;
});
