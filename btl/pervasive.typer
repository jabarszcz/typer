%%% pervasive --- Always available definitions

%%      Copyright (C) 2011-2018  Free Software Foundation, Inc.
%%
%% Author: Pierre Delaunay <pierre.delaunay@hec.ca>
%% Keywords: languages, lisp, dependent types.
%%
%% This file is part of Typer.
%%
%% Typer is free software; you can redistribute it and/or modify it under the
%% terms of the GNU General Public License as published by the Free Software
%% Foundation, either version 3 of the License, or (at your option) any
%% later version.
%%
%% Typer is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
%% FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
%% more details.
%%
%% You should have received a copy of the GNU General Public License along
%% with this program.  If not, see <http://www.gnu.org/licenses/>.

%%% Commentary:

%% This file includes all kinds of predefined types and functions that are
%% generally useful.  It plays a similar role to `builtins.typer` and is
%% read right after that one.  The main reason for the separation into
%% 2 different files, is that for technical reasons, one cannot use macros
%% in `builtins.typer`.

%%% Code:

%%%% `Option` type

Option = typecons (Option (a : Type)) (none) (some a);
some = datacons Option some;
none = datacons Option none;

%%%% List functions

List_length : List ?a -> Int;
List_length xs = case xs
  | nil => 0
  | cons hd tl =>
      (1 + (List_length tl));

%% ML's typical `head` function is not total, so can't be defined
%% as-is in Typer.  There are several workarounds:
%% - Provide a default value : `a -> List a -> a`;
%% - Disallow problem case   : `(l : List a) -> (l != nil) -> a`;
%% - Return an Option/Error
List_head1 : List ?a -> Option ?a;
List_head1 : (a : Type) ≡> List a -> Option a;
List_head1 xs = case xs
  | nil => none
  | cons hd tl => some hd;

List_head x = lambda xs -> case (xs : List ?a)
  %% FIXME: We shouldn't need to annotate `xs` above!
  | cons x _ => x
  | nil => x;

List_tail xs = case (xs : List ?a) % FIXME: Same here.
  | nil => nil
  | cons hd tl => tl;

List_map : (?a -> ?b) -> List ?a -> List ?b;
List_map f = lambda xs -> case xs
  | nil => nil
  | cons x xs => cons (f x) (List_map f xs);

List_foldr : (?b -> ?a -> ?a) -> List ?b -> ?a -> ?a;
List_foldr f = lambda xs -> lambda i -> case xs
  | nil => i
  | cons x xs => f x (List_foldr f xs i);

List_find : (?a -> Bool) -> List ?a -> Option ?a;
List_find f = lambda xs -> case xs
  | nil => none
  | cons x xs => case f x | true => some x | false => List_find f xs;

List_nth : Int -> List ?a -> ?a -> ?a;
List_nth = lambda n -> lambda xs -> lambda d -> case xs
  | nil => d
  | cons x xs
    => case Int_<= n 0
       | true => x
       | false => List_nth (n - 1) xs d;

%%%% A more flexible `lambda`

%% An `Sexp` which we use to represents an *error*.
Sexp_error = Sexp_symbol "<error>";

%% Sexp_to_list : Sexp -> List Sexp -> List Sexp;
Sexp_to_list s = lambda exceptions ->
  let singleton (_ : ?) = cons s nil in %FIXME: Get rid of ": ?"!
  Sexp_dispatch
      s
      (node := lambda head -> lambda tail
        -> case List_find (Sexp_eq head) exceptions
        | some _ => singleton ()
        | none => cons head tail)
      (symbol := lambda s
        -> case String_eq "" s
        | true => nil
        | false => singleton ())
      singleton singleton singleton singleton;

multiarg_lambda =
  %% This macro lets `lambda_->_` (and siblings) accept multiple arguments.
  %% TODO: We could additionally turn
  %%     lambda (x :: t) y -> e
  %% into
  %%     lambda (x : t) => lambda y -> e`
  %% thus providing an alternate syntax for lambdas which don't use
  %% => and ≡>.
  let exceptions = List_map Sexp_symbol
                            (cons "_:_" (cons "_::_" (cons "_:::_" nil))) in
  lambda name ->
  let sym = (Sexp_symbol name);
      mklambda = lambda arg -> lambda body ->
        Sexp_node sym (cons arg (cons body nil)) in
  lambda (margs : List Sexp)
  -> IO_return
         case margs
         | nil => Sexp_error
         | cons fargs margstail
           => case margstail
              | nil => Sexp_error
              | cons body bodytail
                => case bodytail
                   | cons _ _ => Sexp_error
                   | nil => List_foldr mklambda
                                       (Sexp_to_list fargs exceptions)
                                       body;

lambda_->_ = macro (multiarg_lambda "##lambda_->_");
lambda_=>_ = macro (multiarg_lambda "##lambda_=>_");
lambda_≡>_ = macro (multiarg_lambda "##lambda_≡>_");

%%%% More list functions

List_reverse : List ?a -> List ?a -> List ?a;
List_reverse l t = case l
  | nil => t
  | cons hd tl => List_reverse tl (cons hd t);

List_concat : List ?a -> List ?a -> List ?a;
List_concat l t = List_reverse (List_reverse l nil) t;

List_foldl : (?a -> ?b -> ?a) -> ?a -> List ?b -> ?a;
List_foldl f i xs = case xs
  | nil => i
  | cons x xs => List_foldl f (f i x) xs;

List_mapi : (a : Type) ≡> (b : Type) ≡> (a -> Int -> b) -> List a -> List b;
List_mapi f xs = let
  helper : (a -> Int -> b) -> Int -> List a -> List b;
  helper f i xs = case xs
    | nil => nil
    | cons x xs => cons (f x i) (helper f (i + 1) xs);
in helper f 0 xs;

List_map2 : (?a -> ?b -> ?c) -> List ?a -> List ?b -> List ?c;
List_map2 f xs ys = case xs
  | nil => nil
  | cons x xs => case ys
    | nil => nil % error
    | cons y ys => cons (f x y) (List_map2 f xs ys);

%% Fold 2 List as long as both List are non-empty
List_fold2 : (?a -> ?b -> ?c -> ?a) -> ?a -> List ?b -> List ?c -> ?a;
List_fold2 f o xs ys = case xs
  | cons x xs => ( case ys
    | cons y ys => List_fold2 f (f o x y) xs ys
    | nil => o ) % may be an error
  | nil => o; % may or may not be an error

%% Is argument List empty?
List_empty : List ?a -> Bool;
List_empty xs = Int_eq (List_length xs) 0;

%%% Good 'ol combinators

I : ?a -> ?a;
I x = x;

%% Use explicit erasable annotations since with an annotation like
%%     K : ?a -> ?b -> ?a;
%% Typer generalizes to
%%     K : (a : Type) ≡> (b : Type) ≡> a -> b -> a;
%% Which is less general.
%% FIXME: Change the generalizer to insert the `(b : Type) ≡>`
%% more lazily (i.e. after the `a` argument).
K : ?a -> (b : Type) ≡> b -> ?a;
K x y = x;

%%%% Quasi-Quote macro

%%   f = (quote (uquote x) * x) (node _*_ [(Sexp_node unquote "x") "x"])
%%
%%   f = Sexp_node "*" cons (x, cons (Sexp_symbol "x") nil))
%%
%%     =>       x

%% Takes an Sexp `x` as argument and return an Sexp which represents code
%% which will construct an Sexp equivalent to `x` at run-time.
%% This is basically *cross stage persistence* for Sexp.
quote1 : Sexp -> Sexp;
quote1 x = let k = K x;
               qlist : List Sexp -> Sexp;
               qlist xs = case xs
                 | nil => Sexp_symbol "nil"
                 | cons x xs => Sexp_node (Sexp_symbol "cons")
                                          (cons (quote1 x)
                                                (cons (qlist xs) nil));
               node op y = case (Sexp_eq op (Sexp_symbol "uquote"))
                 | true  => List_head Sexp_error y
                 | false => Sexp_node (Sexp_symbol "##Sexp.node")
                                      (cons (quote1 op)
                                            (cons (qlist y) nil));
               symbol s = Sexp_node (Sexp_symbol "##Sexp.symbol")
                                    (cons (Sexp_string s) nil)
           in Sexp_dispatch x node symbol k k k k;

%% quote definition
quote = macro (lambda x -> IO_return (quote1 (List_head Sexp_error x)));

%%%% The `type` declaration macro

%% Build a declaration:
%%     var-name = value-expr;
make-decl : Sexp -> Sexp -> Sexp;
make-decl var-name value-expr =
  Sexp_node (Sexp_symbol "_=_")
            (cons var-name
                  (cons value-expr nil));

chain-decl : Sexp -> Sexp -> Sexp;
chain-decl a b =
  Sexp_node (Sexp_symbol "_;_") (cons a (cons b nil));

chain-decl3 : Sexp -> Sexp -> Sexp -> Sexp;
chain-decl3 a b c =
  Sexp_node (Sexp_symbol "_;_") (cons a (cons b (cons c nil)));

%% Build datacons:
%%     ctor-name = datacons type-name ctor-name;
make-cons : Sexp -> Sexp -> Sexp;
make-cons ctor-name type-name =
  make-decl ctor-name
            (Sexp_node (Sexp_symbol "datacons")
                       (cons type-name
                             (cons ctor-name nil)));

%% Build type annotation:
%%     var-name : type-expr;
make-ann : Sexp -> Sexp -> Sexp;
make-ann var-name type-expr =
  Sexp_node (Sexp_symbol "_:_")
            (cons var-name
                  (cons type-expr nil));

type-impl = lambda (x : List Sexp) ->
  %% `x` follow the mask ->
  %%                  (_|_ Nat zero (succ Nat))
  %%       Type name  --^    ^------^ constructors

  %% Return a list contained inside a node sexp.
  let knil = K nil;
      kerr = K Sexp_error;

      get-list : Sexp -> List Sexp;
      get-list node = Sexp_dispatch node
                                     (lambda op lst -> lst) % Nodes
                                     knil      % Symbol
                                     knil      % String
                                     knil      % Integer
                                     knil      % Float
                                     knil;     % List of Sexp

      %% Get a name (symbol) from a sexp
      %%   - (name t) -> name
      %%   - name -> name
      get-name : Sexp -> Sexp;
      get-name sxp =
        Sexp_dispatch sxp
                       (lambda op _ -> get-name op) % Nodes
                       (lambda _    -> sxp) % Symbol
                       kerr  % String
                       kerr  % Integer
                       kerr  % Float
                       kerr; % List of Sexp

      get-args : Sexp -> List Sexp;
      get-args sxp =
        Sexp_dispatch sxp
                       (lambda _ args -> args)            % Nodes
                       (lambda _    -> (nil : List Sexp)) % Symbol
                       (lambda _ -> (nil : List Sexp))    % String
                       (lambda _ -> (nil : List Sexp))    % Integer
                       (lambda _ -> (nil : List Sexp))    % Float
                       (lambda _ -> (nil : List Sexp));   % List of Sexp

      get-type : Sexp -> Sexp;
      get-type sxp =
        Sexp_dispatch sxp
                      (lambda s ss -> case (Sexp_eq s (Sexp_symbol "_:_"))
                        | true  => List_nth 1 ss Sexp_error
                        | false => sxp)
                      (lambda _ -> sxp)
                      (lambda _ -> Sexp_error)
                      (lambda _ -> Sexp_error)
                      (lambda _ -> Sexp_error)
                      (lambda _ -> Sexp_error);

      get-head = List_head Sexp_error;

      %% Get expression.
      expr = get-head x;

      %% Expression is  Sexp_node (Sexp_symbol "|") (list)
      %% we only care about the list bit
      lst = get-list expr;

      %% head is (Sexp_node type-name (arg list))
      name = get-head lst;
      ctor = List_tail lst;

      type-name = get-name name;

      %% Create the inductive type definition.
      inductive = Sexp_node (Sexp_symbol "typecons")
                            (cons name ctor);

      %% Declare the inductive type as a function
      %% Useful for recursive type
      type-decl = quote ((uquote (get-name name)) : (uquote (
        (List_foldl (lambda args arg ->
          Sexp_node (Sexp_symbol "_->_") (cons (get-type arg) (cons args nil)))
            (Sexp_symbol "Type") (List_reverse (get-args name) nil))
      )));

      decl  = make-decl type-name inductive;

      %% Add constructors.
      ctors = let for-each : List Sexp -> Sexp -> Sexp;
                  for-each ctr acc = case ctr
                    | cons hd tl =>
                        let acc2 = chain-decl (make-cons (get-name hd)
                                                         type-name)
                                              acc
                        in for-each tl acc2
                    | nil => acc
              in for-each ctor (Sexp_node (Sexp_symbol "_;_") nil)

                            %% return decls
  in IO_return (chain-decl3 type-decl % type as a function declaration
                            decl      % inductive type declaration
                            ctors);   % constructor declarations

type_ = macro type-impl;

%%%% Tuples

%% Sample tuple: a module holding Bool and its constructors.
BoolMod = (##datacons
             %% We need the `?` metavars to be lexically outside of the
             %% `typecons` expression, otherwise they end up generalized, so
             %% we end up with a type constructor like
             %%
             %%     typecons _ (cons (τ₁ : Type) (t : τ₁)
             %%                      (τ₂ : Type) (true : τ₂)
             %%                      (τ₃ : Type) (false : τ₃))
             %%
             %% And it's actually even worse because it tries to generalize
             %% over the level of those `Type`s, so we end up with an invalid
             %% inductive type.
             ((lambda t1 t2 t3
               -> typecons _ (cons (t :: t1) (true :: t2) (false :: t3)))
                  ? ? ?)
             cons)
              (_ := Bool) (_ := true) (_ := false);

Pair = typecons (Pair (a : Type) (b : Type)) (pair (fst : a) (snd : b));
pair = datacons Pair pair;

__\.__ =
  let mksel o f =
        let constructor = Sexp_node (Sexp_symbol "##datacons")
                                    (cons (Sexp_symbol "?")
                                          (cons (Sexp_symbol "cons")
                                                nil));
            pattern = Sexp_node constructor
                                (cons (Sexp_node (Sexp_symbol "_:=_")
                                                 (cons f (cons (Sexp_symbol "v")
                                                               nil)))
                                      nil);
            branch = Sexp_node (Sexp_symbol "_=>_")
                               (cons pattern (cons (Sexp_symbol "v") nil));
        in Sexp_node (Sexp_symbol "case_")
                     (cons (Sexp_node (Sexp_symbol "_|_")
                                      (cons o (cons branch nil)))
                           nil)
  in macro (lambda args
            -> IO_return
                   case args
                   | cons o tail
                     => (case tail
                         | cons f _ => mksel o f
                         | nil => Sexp_error)
                   | nil => Sexp_error);

%% Triplet (tuple with 3 values)
type Triplet (a : Type) (b : Type) (c : Type)
  | triplet (x : a) (y : b) (z : c);

%%%% List with tuple

%% Merge two List to a List of Pair
%% Both List must be of same length
List_merge : List ?a -> List ?b -> List (Pair ?a ?b);
List_merge xs ys = case xs
  | cons x xs => ( case ys
    | cons y ys => cons (pair x y) (List_merge xs ys)
    | nil => nil ) % error
  | nil => nil;

%% `Unmerge` a List of Pair
%% The two functions name said it all
List_map-fst : List (Pair ?a ?b) -> List ?a;
List_map-fst xs = let
  mf : Pair ?a ?b -> ?a;
  mf p = case p | pair x _ => x;
in List_map mf xs;

List_map-snd : List (Pair ?a ?b) -> List ?b;
List_map-snd xs = let
  mf : Pair ?a ?b -> ?b;
  mf p = case p | pair _ y => y;
in List_map mf xs;

%%%% Logic

%% `False` should be one of the many empty types.
%% Other popular choices are False = ∀a.a and True = ∃a.a.
False = Void;
True = Unit;

Not : Type -> Type;
Not prop = prop -> False;

%% Like Bool, except that it additionally carries the meaning of its value.
%% We don't use the `type` macro here because it would make these `true`
%% and `false` constructors ovrride `Bool`'s, and we currently don't
%% want that.
Decidable = typecons (Decidable (prop : Type))
                     (true (p ::: prop)) (false (p ::: Not prop));

%% Testing generalization in inductive type constructors.
LList : (a : Type) -> Int -> Type; %FIXME: `LList : ?` should be sufficient!
type LList (a : Type) (n : Int)
  | vnil (p ::: Eq n 0)
  | vcons a (LList a ?n1) (p ::: Eq n (?n1 + 1)); %FIXME: Unsound wraparound!

%%%% If

define-operator "if" () 2;
define-operator "then" 2 1;
define-operator "else" 1 66;

if_then_else_
  = macro (lambda args ->
           let e1 = List_nth 0 args Sexp_error;
               e2 = List_nth 1 args Sexp_error;
               e3 = List_nth 2 args Sexp_error;
           in IO_return (quote (case uquote e1
                                | true => uquote e2
                                | false => uquote e3)));

%%%% More boolean

%% FIXME: Type annotations should not be needed below.
not : Bool -> Bool;
%% FIXME: We use braces here to delay parsing, otherwise the if/then/else
%% part of the grammar declared above is not yet taken into account.
not x = { if x then false else true };

or : Bool -> Bool -> Bool;
or x y = { if x then x else y };

and : Bool -> Bool -> Bool;
and x y = { if x then y else x };

xor : Bool -> Bool -> Bool;
xor x y = { if x then (not y) else y };

%% FIXME: Can't use ?a because that is generalized to be universe-polymorphic,
%% which we don't support in `load` yet.
fold : Bool -> (a : Type) ≡> a -> a -> a;
fold x t e = { if x then t else e};

%%%% Test
test1 : ?;
test2 : Option Int;
test3 : Option Int;
test1 = test2;
%% In earlier versions of Typer, we could use `?` in declarations
%% to mean "fill this for me by unifying with later type information",
%% but this is incompatible with the use of generalization to introduce
%% implicit arguments.
%%     test4 : Option ?;
test2 = none;
test4 : Option Int;
test4 = test1 : Option ?;
test3 = test4;

%%%% Rewrite of some builtins to use `Option`

%%
%% If `Elab_nth-arg'` returns "_" it means:
%%   A- The constructor isn't defined, or
%%   B- The constructor has no n'th argument
%%
%% So in those case this function returns `none`
%%
Elab_nth-arg a b c = let
  r = Elab_nth-arg' a b c;
in case (String_eq r "_")
     | true  => (none)
     | false => (some r);

%%
%% If `Elab_arg-pos'` returns (-1) it means:
%%   A- The constructor isn't defined, or
%%   B- The constructor has no argument named like this
%%
%% So in those case this function returns `none`
%%
Elab_arg-pos a b c = let
  r = Elab_arg-pos' a b c;
in case (Int_eq r (-1))
     | true  => (none)
     | false => (some r);

%%%%
%%%% Common library
%%%%

%%
%% `<-` operator used in macro `do` and for tuple assignment
%%

define-operator "<-" 80 96;

%%
%% `List` is the type and `list` is the module
%%
list = load "btl/list.typer";

%%
%% Macro `do` for easier series of IO operation
%%
%% e.g.:
%%     do { IO_return true; };
%%
do = let lib = load "btl/do.typer" in lib.do;

%%
%% Module containing various macros for tuple
%% Used by `case_`
%%
tuple-lib = load "btl/tuple.typer";

%%
%% Get the nth element of a tuple
%%
%% e.g.:
%%     tuple-nth tup 0;
%%
tuple-nth = tuple-lib.tuple-nth;

%%
%% Affectation of tuple
%%
%% e.g.:
%%     (x,y,z) <- tup;
%%
_<-_ = tuple-lib.assign-tuple;

%%
%% Instantiate a tuple from expressions
%%
%% e.g.:
%%     tup = (x,y,z);
%%
_\,_ = tuple-lib.make-tuple;

Tuple = tuple-lib.tuple-type;

%%
%% Macro `case` for a some more complex pattern matching
%%
case_ = let lib = load "btl/case.typer" in lib.case-macro;

%%
%% plain-let
%% Not recursive and not sequential
%%
%% e.g.:
%%     plain-let x = 1; in x;
%%

define-operator "plain-let" () 3;

%%
%% Already defined:
%%     define-operator "in" 3 67;
%%

plain-let_in_ = let lib = load "btl/plain-let.typer" in lib.plain-let-macro;

%%
%% `case` at function level
%%
%% e.g.:
%%     my-fun (x : Bool)
%%       | true  => false
%%       | false => true;
%%
_|_ = let lib = load "btl/polyfun.typer" in lib._|_;

%%%% Unit tests function for doing file

%% It's hard to do a primitive which execute test file
%% because of circular dependency...

%%
%% A macro using `load` for unit testing purpose
%%
%% Takes a file name (String) as argument
%% Returns variable named `exec-test` from the loaded file
%%
%% `exec-test` should be a command doing unit tests
%% for other purpose than that just use `load` directly!
%%
Test_file = macro (lambda args -> let

  %% return an empty command on error
  ret = Sexp_node
    (Sexp_symbol "IO_return")
    (cons (Sexp_symbol "unit") nil);

%% Expecting a String and nothing else
in IO_return (Sexp_dispatch (List_nth 0 args Sexp_error)
    (lambda _ _ -> ret)
    (lambda _ -> ret)
    %% `load` is a special form and takes a Sexp rather than a real `String`
    (lambda s -> quote (let lib = load (uquote (Sexp_string s)); in lib.exec-test))
    (lambda _ -> ret)
    (lambda _ -> ret)
    (lambda _ -> ret))
);

%%% pervasive.typer ends here.
