#!/bin/sh

# I saw _history in REPL.ml and I'm not sure if there's a
# simple way to do it portably. Anyway this solution (for linux) seemed so simple
# that I had to try it.

# Alternative for history, user input, etc. are ledit which isn't on my base
# repository or ncurses which is a dependency and not a wrapper.

# complete-filenames is useful for "%readfile ..." command
# history-no-dupes just make it faster to search history with up and down key
rlwrap --complete-filenames --history-no-dupes 1 ./_build/typer $*

# if not specified history must be kept in "~/.typer_history"
