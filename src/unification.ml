(* unification.ml --- Unification of Lexp terms

Copyright (C) 2016-2018  Free Software Foundation, Inc.

Author: Vincent Bonnevalle <tiv.crb@gmail.com>

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Lexp
(* open Sexp *)
(* open Inverse_subst *)
module OL = Opslexp
module DB = Debruijn

(** Provide unify function for unifying two Lambda-Expression *)

(* :-( *)
let global_last_metavar = ref (-1) (*The first metavar is 0*)

let create_metavar (ctx : DB.lexp_context) (sl : scope_level) (t : ltype)
  = let idx = !global_last_metavar + 1 in
    global_last_metavar := idx;
    metavar_table := U.IMap.add idx (MVar (sl, t, Myers.length ctx))
                               (!metavar_table);
    idx

(* For convenience *)
type return_type = constraints option

(** Alias for VMap.add*)
let associate (id: meta_id) (lxp: lexp) (subst: meta_subst) : meta_subst
  = U.IMap.add id (MVal lxp) subst

let occurs_in (id: meta_id) (e : lexp) : bool = match metavar_lookup id with
  | MVal _ -> U.internal_error
               "Checking occurrence of an instantiated metavar!!"
  | MVar (sl, _, _)
    -> let rec oi e = match e with
        | Imm _ -> false
        | SortLevel SLz -> false
        | SortLevel (SLsucc e) -> oi e
        | SortLevel (SLlub (e1, e2)) -> oi e1 || oi e2
        | Sort (_, Stype e) -> oi e
        | Sort (_, (StypeOmega | StypeLevel)) -> false
        | Builtin _ -> false
        | Var (_, i) -> false
        | Susp (e, s) -> U.internal_error "`e` should be \"clean\" here!?"
                      (* ; oi (push_susp e s) *)
        | Let (_, defs, e)
          -> List.fold_left (fun o (_, e, t) -> o || oi e || oi t) (oi e) defs
        | Arrow (_, _, t1, _, t2) -> oi t1 || oi t2
        | Lambda (_, _, t, e) -> oi t || oi e
        | Call (f, args)
          -> List.fold_left (fun o (_, arg) -> o || oi arg) (oi f) args
        | Inductive (_, _, args, cases)
          -> SMap.fold
              (fun _ fields o
               -> List.fold_left (fun o (_, _, t) -> o || oi t)
                   o fields)
              cases
              (List.fold_left (fun o (_, _, t) -> o || oi t) false args)
        | Cons (t, _) -> oi t
        | Case (_, e, t, cases, def)
          -> let o = oi e || oi t in
            let o = match def with | None -> o | Some (_, e) -> o || oi e in
            SMap.fold (fun _ (_, _, e) o -> o || oi e)
              cases o
        | Metavar (id', _, name) when id' = id -> true
        | Metavar (id', _, _)
          -> (match metavar_lookup id' with
             | MVal e -> U.internal_error "`e` should be \"clean\" here!?"
                      (* ; oi e *)
             | MVar (sl', t, cl)
               -> if sl' > sl then
                   (* id' will now appear in id's scope, so if id's scope is
                    * higher than id', we need to make sure id' won't be
                    * generalized in sl' but only in sl!
                    * This is the trick mentioned in
                    * http://okmij.org/ftp/ML/generalization.html
                    * to avoid computing `fv lctx` when generalizing!  *)
                   metavar_table := U.IMap.add id' (MVar (sl, t, cl))
                                     (!metavar_table);
                 false) in
      let old_mvt = (!metavar_table) in
      if oi e then
        (* Undo the side-effects since we're not going to instantiate the
           var after all!  *)
        (metavar_table := old_mvt; true)
      else false
      
(**
 lexp is equivalent to _ in ocaml
 (Let , lexp) == (lexp , Let)
 UNIFY      -> recursive call or dispatching
 OK         -> add a substituion to the list of substitution
 CONSTRAINT -> returns a constraint
*)

let unify_and res op = match res with
  | None -> None
  | Some constraints1
    -> match op with
      | None -> None
      | Some constraints2 -> Some (constraints2@constraints1)

(****************************** Top level unify *************************************)

(** Dispatch to the right unifier.

 If (<code>unify_X X Y</code>) don't handle the case <b>(X, Y)</b>, it call (<code>unify Y X</code>)

 The metavar unifier is the end rule, it can't call unify with its parameter (changing their order)
*)
let rec unify (e1: lexp) (e2: lexp)
              (ctx : DB.lexp_context)
        : return_type =
  unify' e1 e2 ctx OL.set_empty

and unify' (e1: lexp) (e2: lexp)
           (ctx : DB.lexp_context) (vs : OL.set_plexp)
    : return_type =
  let e1' = OL.lexp_whnf e1 ctx in
  let e2' = OL.lexp_whnf e2 ctx in
  let changed = not (e1 == e1' && e2 == e2') in
  if changed && OL.set_member_p vs e1' e2' then Some [] else
  let vs' = if changed then OL.set_add vs e1' e2' else vs in
  match (e1', e2') with
    | ((Imm _, Imm _) | (Cons _, Cons _) | (Builtin _, Builtin _)
       | (Var _, Var _) |  (Inductive _, Inductive _))
      -> if OL.conv_p ctx e1' e2' then Some [] else None
    | (l, (Metavar (idx, s, _) as r)) -> unify_metavar ctx idx s r l
    | ((Metavar (idx, s, _) as l), r) -> unify_metavar ctx idx s l r
    | (l, (Call _ as r))    -> unify_call     r l ctx vs'
    (* | (l, (Case _ as r))    -> unify_case     r l subst *)
    | (Arrow _ as l, r)     -> unify_arrow    l r ctx vs'
    | (Lambda _ as l, r)    -> unify_lambda   l r ctx vs'
    | (Call _ as l, r)      -> unify_call     l r ctx vs'
    (* | (Case _ as l, r)      -> unify_case     l r subst *)
    (* | (Inductive _ as l, r) -> unify_induct   l r subst *)
    | (Sort _ as l, r)      -> unify_sort     l r ctx vs'
    | (SortLevel _ as l, r) -> unify_sortlvl  l r ctx vs'
    | _ -> Some (if OL.conv_p ctx e1' e2' then [] else [(e1, e2)])

(********************************* Type specific unify *******************************)

(** Unify a Arrow and a lexp if possible
 - (Arrow, Arrow) -> if var_kind = var_kind
                     then unify ltype & lexp (Arrow (var_kind, _, ltype, lexp))
                     else None
 - (Arrow, Var) -> Constraint
 - (_, _) -> None
*)
and unify_arrow (arrow: lexp) (lxp: lexp) ctx vs
  : return_type =
  match (arrow, lxp) with
  | (Arrow (var_kind1, v1, ltype1, _, lexp1),
     Arrow (var_kind2, _, ltype2, _, lexp2))
    -> if var_kind1 = var_kind2
      then unify_and (unify' ltype1 ltype2  ctx vs)
                     (unify' lexp1 lexp2
                             (DB.lexp_ctx_cons ctx v1 Variable ltype1)
                             (OL.set_shift vs))
      else None
  | (Arrow _, Imm _) -> None
  | (Arrow _, Var _) -> Some ([(arrow, lxp)])
  | (Arrow _, _) -> unify' lxp arrow ctx vs
  | (_, _) -> None

(** Unify a Lambda and a lexp if possible
 - Lamda     , Lambda             -> if var_kind = var_kind
                                     then UNIFY ltype & lxp else ERROR
 - Lambda    , Var                -> CONSTRAINT
 - Lambda    , Call               -> Constraint
 - Lambda    , Let                -> Constraint
 - Lambda    , lexp               -> unify lexp lambda subst
*)
and unify_lambda (lambda: lexp) (lxp: lexp) ctx vs : return_type =
  match (lambda, lxp) with
  | (Lambda (var_kind1, v1, ltype1, lexp1),
     Lambda (var_kind2, _, ltype2, lexp2))
    -> if var_kind1 = var_kind2
      then unify_and (unify' ltype1 ltype2  ctx vs)
                     (unify' lexp1 lexp2
                             (DB.lexp_ctx_cons ctx v1 Variable ltype1)
                             (OL.set_shift vs))
      else None
  | ((Lambda _, Var _)
     | (Lambda _, Let _)
     | (Lambda _, Call _))  -> Some [(lambda, lxp)]
  | (Lambda _, Arrow _) -> None
  | (Lambda _, Imm _)   -> None
  | (Lambda _, _)       -> unify' lxp lambda ctx vs
  | (_, _)              -> None

(** Unify a Metavar and a lexp if possible
 - lexp      , {metavar <-> none} -> UNIFY
 - lexp      , {metavar <-> lexp} -> UNFIFY lexp subst[metavar]
 - metavar   , metavar            -> if Metavar = Metavar then OK else ERROR
 - metavar   , lexp               -> OK
*)
and unify_metavar ctx idx s (lxp1: lexp) (lxp2: lexp)
    : return_type =
  let unif idx s lxp =
    let t = match metavar_lookup idx with
      | MVal _ -> U.internal_error
                   "`lexp_whnf` returned an instantiated metavar!!"
      | MVar (_, t, _) -> push_susp t s in
    match Inverse_subst.apply_inv_subst lxp s with
    | exception Inverse_subst.Not_invertible
      -> print_string "Unification of metavar failed:\n  ";
                   print_string ("?[" ^ subst_string s ^ "]");
                   print_string "\nAgainst:\n    ";
                   lexp_print lxp;
                   print_string "\n";
                   None
    | lxp' when occurs_in idx lxp' -> None
    | lxp'
      -> metavar_table := associate idx lxp' (!metavar_table);
        match unify t (OL.get_type ctx lxp) ctx with
        | Some [] as r -> r
        (* FIXME: Let's ignore the error for now.  *)
        | _
          -> print_string ("Unification of metavar type failed:\n  "
                          ^ lexp_string (Lexp.clean t) ^ " != "
                          ^ lexp_string (Lexp.clean (OL.get_type ctx lxp))
                          ^ "\n" ^ "for " ^ lexp_string lxp ^ "\n");
            Some [] in
  match lxp2 with
  | Metavar (idx2, s2, _)
    -> if idx = idx2 then
        (* FIXME: handle the case where s1 != s2 !!  *)
        Some []
      else
        (* If one of the two subst can't be inverted, try the other.
         * FIXME: There's probably a more general solution.  *)
        (match unif idx s lxp2 with
         | Some s -> Some s
         | None ->
            match unif idx2 s2 lxp1 with
            | Some s -> Some s
            | None -> None)
  | _ -> unif idx s lxp2

(** Unify a Call (call) and a lexp (lxp)
 - Call      , Call               -> UNIFY
 - Call      , lexp               -> CONSTRAINT
*)
and unify_call (call: lexp) (lxp: lexp) ctx vs
    : return_type =
  match (call, lxp) with
  | (Call (lxp_left, lxp_list1), Call (lxp_right, lxp_list2))
       when OL.conv_p ctx lxp_left lxp_right
    -> List.fold_left (fun op ((ak1, e1), (ak2, e2))
                      -> if ak1 == ak2 then
                          unify_and (unify' e1 e2 ctx vs) op
                        else None)
                     (Some [])
                     (List.combine lxp_list1 lxp_list2)
  | (Call _, _) -> Some [(call, lxp)]
  | (_, _)      -> None

(** Unify a Case with a lexp
 - Case, Case -> try to unify
 - Case, _ -> Constraint
*)
(* and unify_case (case: lexp) (lxp: lexp) (subst: meta_subst) : return_type =
 *   let merge (_, const) subst_res = match subst_res with
 *     | None -> None
 *     | Some (s', c') -> Some (s', const@c')
 *   in
 *   let match_unify_inner lst smap1 smap2 subst =
 *     match unify_inner lst subst with
 *     | None -> None
 *     | Some (s, c) -> merge (s, c) (unify_inner_case (zip (SMap.bindings smap1) (SMap.bindings smap2)) s)
 *   in
 *   let match_lxp_opt lxp_opt1 lxp_opt2 tail smap1 smap2 subst =
 *     match lxp_opt1, lxp_opt2 with
 *     | Some (_, lxp1), Some (_, lxp2)
 *       -> match_unify_inner ((lxp1, lxp2)::tail) smap1 smap2 subst
 *     | _, _ -> None
 *   in
 *   match (case, lxp) with
 *     | (Case (_, lxp, lt12, smap, lxpopt), Case (_, lxp2, lt22, smap2, lxopt2))
 *       -> match_lxp_opt lxpopt lxopt2 ((lt12, lt22)::[]) smap smap2 subst
 *     | (Case _, _)     -> Some (subst, [(case, lxp)])
 *     | (_, _) -> None *)

(** Unify a Inductive and a lexp
 - Inductive, Inductive -> try unify
 - Inductive, Var -> constraint
 - Inductive, Call/Metavar/Case/Let -> constraint
 - Inductive, _ -> None
*)
(* and unify_induct (induct: lexp) (lxp: lexp) (subst: meta_subst) : return_type =
 *   let transform (a, b, c) (d, e, f) = ((a, Some b, c), (d, Some e, f))
 *   and merge map1 map2 (subst, const) : return_type =
 *     match (unify_induct_sub_list (SMap.bindings map1) (SMap.bindings map2) subst) with
 *     | Some (s', c') -> Some (s', const@c')
 *     | None -> None
 *   in
 *   let zip_unify lst subst map1 map2 : return_type =
 *     match unify_inner_induct lst subst with
 *     | None        -> None
 *     | Some (s, c) -> merge map1 map2 (s, c)
 *   in
 *   match (induct, lxp) with
 *   | (Inductive (_, lbl1, farg1, m1), Inductive (_, lbl2, farg2, m2)) when lbl1 = lbl2 ->
 *     (match zip_map farg1 farg2 transform with
 *      | Some [] -> Some (subst, [])
 *      | Some lst -> zip_unify lst subst m1 m2
 *      | None -> None)
 *   | (Inductive _, Var _) -> Some (subst, [(induct, lxp)])
 *   | (_, _) -> None *)

(** Unify a SortLevel with a lexp
 - SortLevel, SortLevel -> if SortLevel ~= SortLevel then OK else ERROR
 - SortLevel, _         -> ERROR
*)
and unify_sortlvl (sortlvl: lexp) (lxp: lexp) ctx vs : return_type =
  match sortlvl, lxp with
  | (SortLevel s, SortLevel s2) -> (match s, s2 with
      | SLz, SLz -> Some []
      | SLsucc l1, SLsucc l2 -> unify' l1 l2 ctx vs
      (* FIXME: Handle SLsub!  *)
      | _, _ -> None)
  | _, _ -> None

(** Unify a Sort and a lexp
 - Sort, Sort -> if Sort ~= Sort then OK else ERROR
 - Sort, Var  -> Constraint
 - Sort, lexp -> ERROR
*)
and unify_sort (sort_: lexp) (lxp: lexp) ctx vs : return_type =
  match sort_, lxp with
  | (Sort (_, srt), Sort (_, srt2)) -> (match srt, srt2 with
      | Stype lxp1, Stype lxp2 -> unify' lxp1 lxp2 ctx vs
      | StypeOmega, StypeOmega -> Some []
      | StypeLevel, StypeLevel -> Some []
      | _, _ -> None)
  | Sort _, Var _ -> Some [(sort_, lxp)]
  | _, _          -> None

(************************ Helper function **************************************)

(***** for Case *****)
(** Check arg_king in <code>(arg_kind * vdef option) list </code> in Case *)
and is_same arglist arglist2 =
  match arglist, arglist2 with
  | (akind, _)::t1, (akind2, _)::t2 when akind = akind2 -> is_same t1 t2
  | [], []                                              -> true
  | _, _                                                -> false

(** try to unify the SMap part of the case *)
(* and unify_inner_case lst subst =
 *   let merge (_, c) res =
 *     match res with
 *     | Some (s', c') -> Some (s', c@c')
 *     | None          -> None
 *   in
 *   let rec unify_inner_case list_ subst =
 *     match list_ with
 *     | ((key, (_, arglist, lxp)), (key2, (_, arglist2, lxp2)))::tail when key = key2 ->
 *       (if is_same arglist arglist2
 *        then ( match unify lxp lxp2 subst with
 *            | Some (s', c) -> merge (s', c) (unify_inner_case tail s')
 *            | None         -> None)
 *        else None)
 *     | [] -> Some (subst, [])
 *     | _ -> None
 *   in (match lst with
 *       | Some [] -> Some (subst, [])
 *       | None -> None
 *       | Some l -> unify_inner_case l subst) *)

(***** for Inductive *****)
(** for unify_induct : unify the formal arg*)
(* and unify_inner_induct lst subst : return_type =
 *   let test ((a1, _, l1), (a2, _, l2)) subst : return_type =
 *     if a1 = a2 then unify l1 l2 subst
 *     else None
 *   in
 *   List.fold_left (fun a e ->
 *       (match a with
 *        | Some (s, c) ->
 *          (match test e s with
 *           | Some (s1, c1) -> Some (s1, c1@c)
 *           | None -> Some (s, c))
 *        | None -> test e subst)
 *     ) None lst *)

(** unify the SMap of list in Inductive *)
(* and unify_induct_sub_list l1 l2 subst =
 *   let test l1 l2 subst =
 *     let merge l1 l2 subst (s, c) =
 *       match (unify_induct_sub_list l1 l2 subst) with
 *       | Some (s1, c1) -> Some (s1, c1@c)
 *       | None -> Some (s, c)
 *     in
 *     let unify_zip lst t1 t2 = match unify_inner_induct lst subst with
 *       | Some (s, c) -> merge l1 l2 subst (s, c)
 *       | None -> (unify_induct_sub_list t1 t2 subst)
 *     in
 *     match l1, l2 with
 *     | (k1, v1)::t1, (k2, v2)::t2 when k1 = k2 ->
 *       (match zip v1 v2 with
 *        | Some [] -> Some (subst, [])
 *        | Some lst -> unify_zip lst t1 t2
 *        | None -> None)
 *     | _, _ -> None
 *   in test l1 l2 subst *)

